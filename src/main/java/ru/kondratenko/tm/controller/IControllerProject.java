package ru.kondratenko.tm.controller;

import ru.kondratenko.tm.dto.response.list.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.ProjectDTO;
import ru.kondratenko.tm.dto.response.ProjectResponseDTO;


public interface IControllerProject {

    ProjectResponseDTO create(ProjectDTO project);

    ProjectResponseDTO updateByIndex(Integer index, ProjectDTO project);

    ProjectResponseDTO updateById(Long id, ProjectDTO project);

    ListProjectResponseDTO viewByName(String name);

    ProjectResponseDTO viewById(Long id);

    ProjectResponseDTO viewByIndex(Integer index);

    ProjectResponseDTO removeByIndex(Integer index);

    ProjectResponseDTO removeById(Long id);

    ListProjectResponseDTO list();

    ListProjectResponseDTO findAllByUserId(Long Id);
}
