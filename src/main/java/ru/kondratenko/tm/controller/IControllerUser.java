package ru.kondratenko.tm.controller;

import ru.kondratenko.tm.dto.UserDTO;
import ru.kondratenko.tm.dto.response.UserResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListUserResponseDTO;


public interface IControllerUser {

    UserResponseDTO create(UserDTO user);

    UserResponseDTO updateByIndex(Integer index, UserDTO user);

    UserResponseDTO updateById(Long id, UserDTO user);

    UserResponseDTO viewByName(String name);

    UserResponseDTO viewById(Long id);

    UserResponseDTO viewByIndex(Integer index);

    UserResponseDTO removeByIndex(Integer index);

    UserResponseDTO removeById(Long id);

    ListUserResponseDTO list();

    ListTaskResponseDTO findTasks(Long id);

    ListProjectResponseDTO findProjects(Long id);

    UserResponseDTO registry(String login, String password);

    UserResponseDTO logOff();
}
