package ru.kondratenko.tm.dto.mapper;

import ru.kondratenko.tm.dto.UserDTO;
import ru.kondratenko.tm.entity.User;

public class UserDTOMapper {
    public static UserDTO toDto(User user) {
        UserDTO userDTO = UserDTO.builder()
                .id(user.getId())
                .name(user.getName())
                .password(user.getPassword())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .role(user.getRole())
                .build();
        return userDTO;
    }
}
