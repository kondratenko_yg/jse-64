package ru.kondratenko.tm.dto.mapper;

import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.entity.Task;

public class TaskDTOMapper {
    public static TaskDTO toDto(Task user) {
        TaskDTO tskDTO = TaskDTO.builder()
                .id(user.getId())
                .name(user.getName())
                .description(user.getDescription())
                .projectId(user.getProject()==null?null:user.getProject().getId())
                .userId(user.getUser() == null? null:user.getUser().getId())
                .build();
        if (user.getDeadline() != null) {
            tskDTO.setDeadline(user.getDeadline().toString());
        }
        return tskDTO;
    }
}
