package ru.kondratenko.tm.dto.mapper;

import ru.kondratenko.tm.dto.ProjectDTO;
import ru.kondratenko.tm.entity.Project;

public class ProjectDTOMapper {
    public static ProjectDTO toDto(Project user) {
        ProjectDTO projectDTO = ProjectDTO.builder()
                .id(user.getId())
                .name(user.getName())
                .description(user.getDescription())
                .userId(user.getUser() == null? null:user.getUser().getId())
                .build();
        return projectDTO;
    }
}
