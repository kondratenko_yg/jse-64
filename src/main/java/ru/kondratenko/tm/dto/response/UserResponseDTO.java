package ru.kondratenko.tm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.kondratenko.tm.dto.UserDTO;
import ru.kondratenko.tm.enumerated.Status;

@Data
@SuperBuilder
public class UserResponseDTO extends ResponseDTO {
    private UserDTO payloadUser;
}
