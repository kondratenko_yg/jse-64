package ru.kondratenko.tm.repository;


import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import ru.kondratenko.tm.entity.Task;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;
@Repository
public class TaskRepository extends CommonRepository<Task>  implements ITaskRepository  {

    public TaskRepository() {
        super(Task.class);
    }

    @Override
    public List<Task> findByName(String name){
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Task> criteriaQuery = criteriaBuilder.createQuery(Task.class);
        Root<Task> root = criteriaQuery.from(Task.class);
        Predicate taskNamePredicate = criteriaBuilder.equal(root.get("name"),name);
        criteriaQuery.where(taskNamePredicate);
        TypedQuery<Task> query = session.createQuery(criteriaQuery);
        List<Task> entity = query.getResultList();
        tx.commit();
        session.close();

        return entity;
    }

    @Override
    public List<Task> findAllByUserId(final Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Task> criteriaQuery = criteriaBuilder.createQuery(Task.class);
        Root<Task> root = criteriaQuery.from(Task.class);
        Predicate taskIdPredicate = criteriaBuilder.equal(root.get("user").get("id"),id);
        TypedQuery<Task> query = session.createQuery(criteriaQuery.where(taskIdPredicate));
        List<Task> entity = query.getResultList();
        tx.commit();
        session.close();

        return entity;
    }

    @Override
    public Optional<Task> findByProjectIdAndId(Long projectId, Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Task> criteriaQuery = criteriaBuilder.createQuery(Task.class);
        Root<Task> root = criteriaQuery.from(Task.class);
        Predicate projectIdPredicate = criteriaBuilder.equal(root.get("project").get("id"),projectId);
        Predicate taskIdPredicate = criteriaBuilder.equal(root.get("id"),id);
        TypedQuery<Task> query = session.createQuery(criteriaQuery.where(criteriaBuilder.and(projectIdPredicate,taskIdPredicate)));
        Task entity = query.getSingleResult();
        tx.commit();
        session.close();

        return Optional.ofNullable(entity);
    }

    @Override
    public List<Task> findAllByProjectId(Long projectId) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Task> criteriaQuery = criteriaBuilder.createQuery(Task.class);
        Root<Task> root = criteriaQuery.from(Task.class);
        Predicate taskIdPredicate = criteriaBuilder.equal(root.get("project").get("id"),projectId);
        TypedQuery<Task> query = session.createQuery(criteriaQuery.where(taskIdPredicate));
        List<Task> entity = query.getResultList();
        tx.commit();
        session.close();

        return entity;
    }
}
