package ru.kondratenko.tm.service;


import ru.kondratenko.tm.dto.response.list.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.response.TaskResponseDTO;

public interface IProjectTaskService {
    TaskResponseDTO addTaskToProject(final Long projectId, final Long taskId);
    TaskResponseDTO removeTaskFromProject(final Long projectId, final Long taskId);
    void clear();
    ListTaskResponseDTO findAllByProjectId(Long projectId);
}
