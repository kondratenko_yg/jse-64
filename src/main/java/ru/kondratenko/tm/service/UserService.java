package ru.kondratenko.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kondratenko.tm.dto.ProjectDTO;
import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.dto.UserDTO;
import ru.kondratenko.tm.dto.mapper.ProjectDTOMapper;
import ru.kondratenko.tm.dto.mapper.TaskDTOMapper;
import ru.kondratenko.tm.dto.mapper.UserDTOMapper;
import ru.kondratenko.tm.dto.response.UserResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListUserResponseDTO;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.enumerated.Status;
import ru.kondratenko.tm.repository.UserRepository;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.kondratenko.tm.util.HashUtil.hashMD5;
@Service
public class UserService implements IUserIService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserResponseDTO create(final UserDTO userDTO) {
        final String login = userDTO.getName(), password = userDTO.getPassword();
        Role role = userDTO.getRole();
        if (login == null || login.isEmpty() || password == null || password.isEmpty()) {
            return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<User> userOptional = userRepository.findByName(login);
        if (userOptional.isPresent()) {
            return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        userOptional = userRepository.create(User.builder()
                .id(userDTO.getId())
                .name(login)
                .password(hashMD5(password))
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .role(role == null ? Role.USER : role).build());
        if (userOptional.isPresent()) {
            return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(userOptional.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO updateByIndex(final int index, final UserDTO userDTO) {
        final String login = userDTO.getName();
        final String password = userDTO.getPassword();
        if (login == null || login.isEmpty() || index < 0) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        if (password == null) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<User> user1 = userRepository.findByIndex(index);
        if (user1.isPresent()) {
            User userUpdate = User.builder()
                    .id(user1.get().getId())
                    .name(login)
                    .password(hashMD5(password))
                    .firstName(userDTO.getFirstName())
                    .lastName(userDTO.getLastName())
                    .build();
            user1 = userRepository.update(userUpdate);
            if (user1.isPresent()) {
                return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(user1.get())).status(Status.OK).build();
            }
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO updateById(final long id, final  UserDTO userDTO) {
        final String login = userDTO.getName();
        final String password = userDTO.getPassword();
        if (login == null || login.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        if (password == null) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<User> user1 = userRepository.findById(id);
        if (user1.isPresent()) {
            User userUpdate = User.builder()
                    .id(user1.get().getId())
                    .name(login)
                    .password(hashMD5(password))
                    .firstName(userDTO.getFirstName())
                    .lastName(userDTO.getLastName())
                    .build();
            user1 = userRepository.update(userUpdate);
            if (user1.isPresent()) {
                return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(user1.get())).status(Status.OK).build();
            }
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO updateByName(final String name, final UserDTO userDTO) {
        final String login = userDTO.getName();
        final String password = userDTO.getPassword();
        if (login == null || login.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        if (password == null) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<User> user1 = userRepository.findByName(name);
        if (user1.isPresent()) {
            User userUpdate = User.builder()
                    .id(user1.get().getId())
                    .name(login)
                    .password(hashMD5(password))
                    .firstName(userDTO.getFirstName())
                    .lastName(userDTO.getLastName())
                    .build();
            user1 = userRepository.update(userUpdate);
            if (user1.isPresent()) {
                return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(user1.get())).status(Status.OK).build();
            }
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public boolean checkPassword(final Optional<User> user, final String password) {
        return user.map(value -> value.getPassword().equals(hashMD5(password))).orElse(false);
    }

    @Override
    public UserResponseDTO findByName(String login) {
        if (login == null || login.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<User> user = userRepository.findByName(login);
        if (user.isPresent()) {
            return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(user.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO findById(Long id) {
        if (id == null) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<User> user = userRepository.findById(id);
        if (user.isPresent()) {
            return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(user.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO findByIndex(final int index) {
        if (index < 0) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<User> user = userRepository.findByIndex(index);
        if (user.isPresent()) {
            return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(user.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO removeByName(String login) {
        if (login == null || login.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        userRepository.removeByName(login);
        return UserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListTaskResponseDTO findTasks(Long id) {
        if (id == null) return ListTaskResponseDTO.builder().status(Status.DB_ERROR).build();
        List<Task> tasks = userRepository.findTasks(id);
        return ListTaskResponseDTO
                .builder()
                .payloadTask(tasks.stream().map(TaskDTOMapper::toDto).toArray(TaskDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public ListProjectResponseDTO findProjects(Long id) {
        if (id == null) return ListProjectResponseDTO.builder().status(Status.DB_ERROR).build();
        List<Project> projects = userRepository.findProjects(id);
        return ListProjectResponseDTO
                .builder()
                .payloadProject(projects.stream().map(ProjectDTOMapper::toDto).toArray(ProjectDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public UserResponseDTO registry(String login, String password) {
        final var user = userRepository.findByName(login);
        if (user.isEmpty()) {
            return UserResponseDTO.builder().status(Status.DB_ERROR).textError("LOGIN IS NOT FOUND IN SYSTEM.").build();
        }
        if (checkPassword(user, password)) {
            setCurrentUser(user.get());
        } else {
            return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        return UserResponseDTO.builder().status(Status.OK).build();

    }

    @Override
    public UserResponseDTO logOff() {
        setCurrentUser(null);
        return UserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public UserResponseDTO removeById(Long id) {
        if (id == null) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        userRepository.removeById(id);
        return UserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public UserResponseDTO removeByIndex(Integer index) {
        if (index < 0) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        userRepository.removeByIndex(index);
        return UserResponseDTO.builder().status(Status.OK).build();

    }

    @Override
    public void clear() {
        userRepository.clear();
    }

    @Override
    public ListUserResponseDTO findAll() {
        return ListUserResponseDTO
                .builder()
                .payloadUser(userRepository.findAll().stream().map(UserDTOMapper::toDto).toArray(UserDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public ListUserResponseDTO saveJSON(final String  fileName) throws IOException {
        writeJSON(fileName,userRepository.findAll().stream().map(UserDTOMapper::toDto).collect(Collectors.toList()));
        return ListUserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListUserResponseDTO saveXML(final String fileName) throws IOException {
        writeXML(fileName,userRepository.findAll().stream().map(UserDTOMapper::toDto).collect(Collectors.toList()));
        return ListUserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListUserResponseDTO uploadFromJSON(final String  fileName) throws IOException {
        List<UserDTO> users = uploadJSONToList(fileName,UserDTO.class);
        clear();
        users.forEach(this::create);
        return ListUserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListUserResponseDTO uploadFromXML(final String  fileName) throws IOException {
        List<UserDTO> users = uploadXMLToList(fileName,UserDTO.class);
        clear();
        users.forEach(this::create);
        return ListUserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public User getCurrentUser() {
        return userRepository.currentUser;
    }

    @Override
    public void setCurrentUser(User user) {
        userRepository.currentUser = user;
    }


}
