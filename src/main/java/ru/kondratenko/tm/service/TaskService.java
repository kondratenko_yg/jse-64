package ru.kondratenko.tm.service;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.dto.mapper.TaskDTOMapper;
import ru.kondratenko.tm.dto.response.TaskResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListTaskResponseDTO;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.enumerated.Status;
import ru.kondratenko.tm.exception.NotFoundException;
import ru.kondratenko.tm.repository.ITaskProjectRepository;
import ru.kondratenko.tm.repository.ITaskRepository;
import ru.kondratenko.tm.repository.UserRepository;
import ru.kondratenko.tm.util.Helper;

import javax.persistence.OptimisticLockException;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Service
public class TaskService implements ITaskIService {

    private ITaskRepository taskRepository;

    private ITaskProjectRepository<Project> projectRepository;

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setTaskRepository(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Autowired
    public void setProjectRepository(ITaskProjectRepository<Project> projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public TaskResponseDTO create(final TaskDTO taskDTO) {
        String name = taskDTO.getName();
        if (name.equals("") || Helper.checkProjectName(name) || name.isEmpty()) {
            return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Task task;
        try {
            task = Task.builder()
                    .name(taskDTO.getName())
                    .description(taskDTO.getDescription())
                    .user(getItemById(taskDTO.getUserId(),userRepository))
                    .project(getItemById(taskDTO.getProjectId(),projectRepository))
                    .build();
        } catch (NotFoundException e) {
            return TaskResponseDTO.builder().status(Status.DB_ERROR).textError(e.getMessage()).build();
        }
        task.setDeadline(getDeadlineFromInput(taskDTO));
        Optional<Task> taskOptional = taskRepository.create(task);
        if (taskOptional.isPresent()) {
            startCountDeadLine(taskOptional.get());
            return TaskResponseDTO.builder().payloadTask(TaskDTOMapper.toDto(taskOptional.get())).status(Status.OK).build();
        }
        return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
    }



    private LocalDateTime getDeadlineFromInput(TaskDTO taskDTO){
        if (taskDTO.getDeadline() != null) {
            try {
                return LocalDateTime.parse(taskDTO.getDeadline());
            } catch (DateTimeParseException e) {
                return LocalDateTime.now().plusMinutes(480L);
            }
        }
        else {
            return LocalDateTime.now().plusMinutes(480L);
        }
    }

    @Override
    public TaskResponseDTO updateByIndex(final int index, TaskDTO task) {
        Optional<Task> task1 = taskRepository.findByIndex(index);
        if (task1.isPresent()) {
            Task updatedTask;
            try {
                updatedTask = Task.builder()
                        .id(task1.get().getId())
                        .name(task.getName())
                        .description(task.getDescription())
                        .user(getItemById(task.getUserId(),userRepository))
                        .project(getItemById(task.getProjectId(),projectRepository))
                        .version(task1.get().getVersion())
                        .build();
            } catch (NotFoundException | OptimisticLockException e) {
                return TaskResponseDTO.builder().status(Status.DB_ERROR).textError(e.getMessage()).build();
            }
            updatedTask.setDeadline(getDeadlineFromInput(task));
            task1 = taskRepository.update(updatedTask);
            if (task1.isPresent()) {
                return TaskResponseDTO.builder().payloadTask(TaskDTOMapper.toDto(task1.get())).status(Status.OK).build();
            }
        }
        return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public TaskResponseDTO updateById(final long id, TaskDTO task) {
        Optional<Task> task1 = taskRepository.findById(id);
        if (task1.isPresent()) {
            Task updatedTask;
            try {
                updatedTask = Task.builder()
                        .id(task1.get().getId())
                        .name(task.getName())
                        .description(task.getDescription())
                        .user(getItemById(task.getUserId(),userRepository))
                        .project(getItemById(task.getProjectId(),projectRepository))
                        .version(task1.get().getVersion())
                        .build();
            } catch (NotFoundException | OptimisticLockException e) {
                return TaskResponseDTO.builder().status(Status.DB_ERROR).textError(e.getMessage()).build();
            }
            updatedTask.setDeadline(getDeadlineFromInput(task));
            task1 = taskRepository.update(updatedTask);
            if (task1.isPresent()) {
                return TaskResponseDTO.builder().payloadTask(TaskDTOMapper.toDto(task1.get())).status(Status.OK).build();
            }
        }
        return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
    }


    @Override
    public TaskResponseDTO findByIndex(final int index) {
        Optional<Task> task = taskRepository.findByIndex(index);
        if (task.isPresent()) {
            return TaskResponseDTO.builder().payloadTask(TaskDTOMapper.toDto(task.get())).status(Status.OK).build();
        }
        return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListTaskResponseDTO findByName(final String name) {
        List<Task> task1 = taskRepository.findByName(name);
        if (task1.isEmpty()) {
            return ListTaskResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        return ListTaskResponseDTO
                .builder()
                .payloadTask(task1.stream().map(TaskDTOMapper::toDto).toArray(TaskDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public TaskResponseDTO findById(final Long id) {
        Optional<Task> task1 = taskRepository.findById(id);
        if (task1.isPresent()) {
            return TaskResponseDTO.builder().payloadTask(TaskDTOMapper.toDto(task1.get())).status(Status.OK).build();
        }
        return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public TaskResponseDTO removeByIndex(final Integer index) {
        taskRepository.removeByIndex(index);
        return TaskResponseDTO.builder().status(Status.OK).build();

    }

    @Override
    public TaskResponseDTO removeById(final Long id) {
        taskRepository.removeById(id);
        return TaskResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public TaskResponseDTO removeByName(final String name) {
        taskRepository.removeByName(name);
        return TaskResponseDTO
                .builder()
                .status(Status.OK).build();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public ListTaskResponseDTO findAll() {
        return ListTaskResponseDTO
                .builder()
                .payloadTask(taskRepository.findAll().stream().map(TaskDTOMapper::toDto).toArray(TaskDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public ListTaskResponseDTO findAllByProjectId(final Long projectId) {

        if (projectId == null) return ListTaskResponseDTO.builder().status(Status.DB_ERROR).build();
        return ListTaskResponseDTO
                .builder()
                .payloadTask(taskRepository.findAllByProjectId(projectId).stream().map(TaskDTOMapper::toDto).toArray(TaskDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public TaskResponseDTO findByProjectIdAndId(final Long projectId, final Long id) {
        if (projectId == null) return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
        if (id == null) return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<Task> task = taskRepository.findByProjectIdAndId(projectId, id);
        if (task.isPresent()) {
            return TaskResponseDTO
                    .builder()
                    .payloadTask(TaskDTOMapper.toDto(task.get()))
                    .status(Status.OK).build();
        }
        return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListTaskResponseDTO findAllByUserId(final Long userId) {
        if (userId == null) return ListTaskResponseDTO.builder().status(Status.DB_ERROR).build();
        return ListTaskResponseDTO
                .builder()
                .payloadTask(taskRepository.findAllByUserId(userId).stream().map(TaskDTOMapper::toDto).toArray(TaskDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public ListTaskResponseDTO saveJSON(final String  fileName) throws IOException {
        writeJSON(fileName,taskRepository.findAll().stream().map(TaskDTOMapper::toDto).collect(Collectors.toList()));
        return ListTaskResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListTaskResponseDTO saveXML(final String fileName) throws IOException {
        writeXML(fileName,taskRepository.findAll().stream().map(TaskDTOMapper::toDto).collect(Collectors.toList()));
        return ListTaskResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListTaskResponseDTO uploadFromJSON(final String  fileName) throws IOException {
        List<TaskDTO> tasks = uploadJSONToList(fileName,TaskDTO.class);
        clear();
        tasks.forEach(this::create);
        return ListTaskResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListTaskResponseDTO uploadFromXML(final String  fileName) throws IOException {
        List<TaskDTO> tasks = uploadXMLToList(fileName,TaskDTO.class);
        clear();
        tasks.forEach(this::create);
        return ListTaskResponseDTO.builder().status(Status.OK).build();
    }

    private void startCountDeadLine(Task task)  {
        CompletableFuture<Long> future = CompletableFuture.supplyAsync(() -> {
            long diff = ChronoUnit.SECONDS.between(LocalDateTime.now(),task.getDeadline());
            long secondsToCheck = (diff+1)/2;
            while(diff > 0){
                diff = ChronoUnit.SECONDS.between(LocalDateTime.now(),task.getDeadline());
                if(diff % secondsToCheck == 0){
                    System.out.println("Time for task " + task.getName() + " - " + DurationFormatUtils.formatDuration(TimeUnit.SECONDS.toMillis(diff), "**H:mm:ss**", true));
                    try {
                        if(secondsToCheck <= diff){
                            TimeUnit.SECONDS.sleep(secondsToCheck-1);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(secondsToCheck>3600){
                        secondsToCheck = secondsToCheck/4;
                    }
                    if(secondsToCheck>30){
                        secondsToCheck = secondsToCheck/2;
                    }

                }
            }
            return diff;
        });

        future.thenAccept(result -> {
            if(userRepository.currentUser != null){
                synchronized (findAllByUserId(userRepository.currentUser.getId())) {
                        removeById(task.getId());
                        System.out.println(task.getName()+" was removed.");
                }
            }
        });

    }


}
