package ru.kondratenko.tm.repository;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.kondratenko.tm.entity.Task;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TaskRepositoryIntegrationTest {

    private static final String NAME_FIND = "nameFind";
    private static final String NAME_DELETE = "nameDelete";
    private static final String NAME_UPDATE = "nameUpdate";
    private static final String NAME_UPDATE2 = "nameUpdate2";
    private static final String DESCRIPTION_UPDATE2 = "descrictionUpdate2";

    private static Optional<Task> resultFind;
    private static Optional<Task> resultDelete;
    private  static TaskRepository taskRepository;
    private static int counter = 0;

    @BeforeAll
    public static void setup() {
        taskRepository = new TaskRepository();
        Task task = Task.builder().name(NAME_FIND).build();
        Task TaskDelete = Task.builder().name(NAME_DELETE + counter++).build();
        resultFind = taskRepository.create(task);
        resultDelete = taskRepository.create(TaskDelete);
    }

    @Test
    void FindByID() {
        if (resultFind.isPresent()) {
            assertEquals(NAME_FIND, taskRepository.findById(resultFind.get().getId()).get().getName());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void FindByIndex() {
        if (resultFind.isPresent()) {
            assertEquals(NAME_FIND, taskRepository.findByIndex(1).get().getName());
        } else {
            throw new RuntimeException();
        }
    }

    @Test  //jse-61
    void FindByName() {
        if (resultFind.isPresent()) {
            assertEquals(NAME_FIND, taskRepository.findByName(NAME_FIND).get(0).getName());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void createAndDeleteById() {
        Task task = Task.builder().name(NAME_DELETE + counter++).build();
        Optional<Task> result = taskRepository.create(task);
        if (result.isPresent()) {
            Long TaskID = result.get().getId();
            taskRepository.removeById(TaskID);
            assertFalse(taskRepository.findById(TaskID).isPresent());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void createAndDeleteByIndex() {
        if (resultDelete.isPresent()) {
            Long TaskID = resultDelete.get().getId();
            taskRepository.removeByIndex(2);
            assertFalse(taskRepository.findById(TaskID).isPresent());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void createAndDeleteByName() {
        String name = NAME_DELETE + counter++;
        Task task = Task.builder().name(name).build();
        Optional<Task> result = taskRepository.create(task);
        if (result.isPresent()) {
            Long TaskID = result.get().getId();
            taskRepository.removeByName(name);
            assertFalse(taskRepository.findById(TaskID).isPresent());
        } else {
            throw new RuntimeException();
        }
    }

    @Test
    void createAndUpdate() {
        Task task = Task.builder().name(NAME_UPDATE).build();
        taskRepository.create(task);
        task.setName(NAME_UPDATE2);
        task.setDescription(DESCRIPTION_UPDATE2);
        Optional<Task> result = taskRepository.update(task);
        if (result.isPresent()) {
            assertEquals(NAME_UPDATE2, taskRepository.findById(result.get().getId()).get().getName());
            assertEquals(DESCRIPTION_UPDATE2, taskRepository.findById(result.get().getId()).get().getDescription());
        } else {
            throw new RuntimeException();
        }
    }

    @AfterAll
    static void  ClearFindAll() {
        taskRepository.clear();
        List<Task> Tasks = taskRepository.findAll();
        assertEquals(0,Tasks.size());
    }


}
