## Операционные системы
Windows, Mac, Linux

## Програмное обеспечение
Java 11.0.7, Apache Maven 3.6.1

## Сборка проекта
```bash
mvn install
```

## Разработчик
Кондратенко Ю.Г. (kondratenko_yg@nlmk.com)

## Запуск приложения
```bash
java -war ./task-manager.war
```

## Сборка Docker образа локально
```bash
docker build . -t tm-java
```

## Запуск локального Docker образа
```bash
docker run -p 8080:8080 tm-java:latest
```
