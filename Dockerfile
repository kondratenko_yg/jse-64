FROM maven:3-jdk-11 as builder

RUN mkdir -p /build
WORKDIR /build
COPY pom.xml /build
COPY src /build/src

EXPOSE 8080
EXPOSE 5432

RUN mvn clean package

COPY myscript.sh /bin/myscript.sh
ENTRYPOINT ["/bin/myscript.sh"]